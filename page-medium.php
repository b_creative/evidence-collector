<?php
// Template Name: Medium Layout Template

$context = Timber::get_context();
$post = Timber::get_post();
$context['post'] = $post;

$templates = ['page-medium.twig'];

Timber::render( $templates, $context );