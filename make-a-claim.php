<?php
// Template Name: Claim Builder Template

$context = Timber::get_context();
$post = Timber::get_post();
$context['post'] = $post;

// frontend claim builder form
// When submitted, a new claim post is created and indexed in FacetWP
function build_new_claim() {
	acf_form([
		'post_id' => 'new_post',
		'new_post' => [
			'post_type' => 'claim',
			'post_status' => 'publish',
		],
		'uploader' => 'basic',
		'post_content' => false,
		'submit_value' => __('SAVE NEW CLAIM', 'acf'),
		'updated_message' => 'Your claim was successfully created and saved.'
	]);
}

$templates = ['make-a-claim.twig'];

Timber::render( $templates, $context );