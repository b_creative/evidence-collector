<?php
$labels = [
	'name'                => __( 'Claims', 'mod' ),
	'singular_name'       => __( 'Claim', 'mod' ),
	'add_new'             => _x( 'New Claim', 'mod', 'mod' ),
	'add_new_item'        => __( 'Add Claim', 'mod' ),
	'edit_item'           => __( 'Edit Claim', 'mod' ),
	'new_item'            => __( 'New Claim', 'mod' ),
	'view_item'           => __( 'View Claim', 'mod' ),
	'search_items'        => __( 'Search Claims', 'mod' ),
	'not_found'           => __( 'No Claims found', 'mod' ),
	'not_found_in_trash'  => __( 'No Claims found in Trash', 'mod' ),
	'parent_item_colon'   => __( 'Parent Claims:', 'mod' ),
	'menu_name'           => __( 'Claims', 'mod' ),
];

$args = [
	'labels'              => $labels,
	'hierarchical'        => true,
	'description'         => '',
	'taxonomies'          => [],
	'public'              => true,
	'show_ui'             => true,
	'show_in_menu'        => true,
	'show_in_admin_bar'   => true,
	'show_in_rest'		  => true,
	'menu_position'       => null,
	'menu_icon'           => 'dashicons-edit-large',
	'show_in_nav_menus'   => false,
	'publicly_queryable'  => true,
	'exclude_from_search' => false,
	'has_archive'         => false,
	'query_var'           => true,
	'can_export'          => true,
	'rewrite'             => true,
	'capability_type'     => 'post',
	'supports'            => [ 'title', 'author' ]
];
register_post_type( 'claim', $args );