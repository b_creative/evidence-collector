<?php
$labels = [
	'name'                => __( 'Address Book', 'mod' ),
	'singular_name'       => __( 'Address Book', 'mod' ),
	'add_new'             => _x( 'New Entry', 'mod', 'mod' ),
	'add_new_item'        => __( 'Add Entry', 'mod' ),
	'edit_item'           => __( 'Edit Entry', 'mod' ),
	'new_item'            => __( 'New Entry', 'mod' ),
	'view_item'           => __( 'View Address Book', 'mod' ),
	'search_items'        => __( 'Search Address Books', 'mod' ),
	'not_found'           => __( 'No Address Books found', 'mod' ),
	'not_found_in_trash'  => __( 'No Address Books found in Trash', 'mod' ),
	'parent_item_colon'   => __( 'Parent Address Book:', 'mod' ),
	'menu_name'           => __( 'Address Book', 'mod' ),
];

$args = [
	'labels'              => $labels,
	'hierarchical'        => false,
	'description'         => '',
	'taxonomies'          => [],
	'public'              => false,
	'show_ui'             => true,
	'show_in_menu'        => true,
	'show_in_admin_bar'   => true,
	'show_in_rest'		  => true,
	'menu_position'       => null,
	'menu_icon'           => 'dashicons-book-alt',
	'show_in_nav_menus'   => false,
	'publicly_queryable'  => true,
	'exclude_from_search' => false,
	'has_archive'         => false,
	'query_var'           => true,
	'can_export'          => true,
	'rewrite'             => true,
	'capability_type'     => 'post',
	'supports'            => [ 'title', 'author' ]
];
register_post_type( 'addressbook', $args );