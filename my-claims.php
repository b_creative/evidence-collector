<?php
// Template Name: My Claims Template

$context = Timber::get_context();
$post = Timber::get_post();
$context['post'] = $post;

// get the current User object
$current_user = wp_get_current_user();

// get the current users posts
$context['user_claims'] = Timber::get_posts([
	'post_type' => 'claim',
	'posts_per_page' => -1,
	'post_status' => 'publish',
	'author' => $current_user->ID
]);

$templates = ['my-claims.twig'];

Timber::render( $templates, $context );