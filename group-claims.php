<?php
// Template Name: Group Claims Template

$context = Timber::get_context();
$post = Timber::get_post();
$context['post'] = $post;

function sorted_group_claims_table() {
	$reps = get_users('role=editor'); // get our claim reps (admins are excluded)

	foreach( $reps as $rep ) {
		$posts = Timber::get_posts([
			'post_type' => 'claim',
			'posts_per_page' => -1,
			'author' => $rep->ID,
			'meta_key' => 'claim_status',
			'orderby' => 'meta_value', // custom sort order
		]);

		echo '<h4 class="rep-name">' . $rep->display_name . '</h4>';

		if( is_wp_error($posts) ) {
			return;
		} ?>

		<table class="display claim-table">
			<thead>
				<tr>
					<th>Claim Number</th>
					<th>Company Name</th>
					<th>Insured Name</th>
					<th>Claim Type</th>
					<th>Claim Status</th>
					<th>Reserve</th>
					<th>View Claim</th>
				</tr>
			</thead>
			<tbody>
		<?php

		// loop through each reps posts
		foreach( $posts as $post ) {
			$pid = $post->ID;

			// claim info & column order
			$claim_number = get_field( 'claim_number', $pid );
			$company_name = get_field( 'company_name', $pid );
			$insured_name = get_field( 'insured_name', $pid );
			$claim_rep = $rep->display_name;
			$reserve = get_field( 'reserve', $pid );
			$claim_type = get_field( 'claim_type', $pid );
			$claim_status = get_field( 'claim_status', $pid );

			// insert the rep info into the row ?>
			<tr>
				<td><a href="<?= $post->link; ?>"><?= $claim_number; ?></a></td>
				<td><a href="<?= $post->link; ?>"><?= $company_name; ?></a></td>
				<td><a href="<?= $post->link; ?>"><?= $insured_name; ?></a></td>
				<td><a href="<?= $post->link; ?>"><?= $claim_type; ?></a></td>
				<td><a href="<?= $post->link; ?>"><?= $claim_status; ?></a></td>
				<td><a href="<?= $post->link; ?>"><?= $reserve; ?></a></td>
				<td><a href="<?= $post->link; ?>" class="view-claim">View Claim</a></td>
			</tr>
			<?php
		} // .end $posts loop
		?></tbody></table><?php
	} // .end $reps loop
}

// Claim Search Function - load all data into a single table\
// We will slice it later into user-defined chunks on search
$context['table_claims'] = Timber::get_posts([
	'post_type' => 'claim',
	'posts_per_page' => -1,
	'post_status' => 'publish',
	'facetwp' => true
]);

$templates = ['group-claims.twig'];

Timber::render( $templates, $context );