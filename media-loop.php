<?php
// Images
if( have_rows('photo_uploads') ) { // loop through images for this claim
	while( have_rows('photo_uploads') ) : the_row();
		$image = get_sub_field( 'photo' );
		$image_url = $image['url'];

		$choices[] = [
			'text' => '<img src="' . $image_url . '" style="width: 100px; height: 100px;" />' . ' ' .
			'<p>' . $image_url . '</p>',
			'value' => $image_url
		];

		$inputs[] = [
			'label' => $image_url,
			'id' => "{$field_id}.{$input_id}"
		];

		$input_id++;
	endwhile;
}

// Videos
if( have_rows('video_uploads') ) { // loop through videos for this claim
	while( have_rows('video_uploads') ) : the_row();
		$video = get_sub_field( 'video_file' );

		$choices[] = [
			'text' => '<img src="' . get_template_directory_uri() . '/assets/media/video-placeholder.jpg" style="width: 70px; height: 100px;" />' . ' ' . '<p>' . $video . '</p>',
			'value' => $video
		];

		$inputs[] = [
			'label' => $video,
			'id' => "{$field_id}.{$input_id}"
		];

		$input_id++;
	endwhile;
}

// Audio
// if( have_rows('audio_uploads') ) { // loop through audio files for this claim
// 	while( have_rows('audio_uploads') ) : the_row();
// 		$audio = get_sub_field( 'audio_file' );

// 		$choices[] = [
// 			'text' => '<img src="' . get_template_directory_uri() . '/assets/media/audio-placeholder.jpg" style="width: 70px; height: 100px;" />' . ' ' . '<p>' . $audio . '</p>',
// 			'value' => $audio
// 		];

// 		$inputs[] = [
// 			'label' => $audio,
// 			'id' => "{$field_id}.{$input_id}"
// 		];

// 		$input_id++;
// 	endwhile;
// }

// Interviews
if( have_rows('interview_uploads') ) { // loop through interview files for this claim
	while( have_rows('interview_uploads') ) : the_row();
		$interview = get_sub_field( 'file_upload' );

		$choices[] = [
			'text' => '<img src="' . get_template_directory_uri() . '/assets/media/interview-placeholder.jpg" style="width: 70px; height: 100px;" />' . ' ' . '<p>' . $interview . '</p>',
			'value' => $interview
		];

		$inputs[] = [
			'label' => $interview,
			'id' => "{$field_id}.{$input_id}"
		];

		$input_id++;
	endwhile;
}