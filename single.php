<?php
// Global Post Type Controller

$context = Timber::get_context();
$post = Timber::get_post();
$context['post'] = $post;

function edit_claim_post() {
	$pid = $post->ID; // current post id
	acf_form([
		'id' => 'edit-form',
		'post_id' => $pid, // set to current post ID (this returns TRUE since we are editing, not creating).
		'uploader' => 'basic',
		'submit_value' => __('Update Claim', 'acf'),
		'updated_message' => 'Claim updated and saved.',
	]);
}

// Update Contact
function edit_contact() {
	$pid = $post->ID; // current post id
	acf_form([
		'id' => 'edit-form',
		'post_id' => $pid, // set to current post ID (this returns TRUE since we are editing, not creating).
		'submit_value' => __('Update Contact', 'acf'),
		'updated_message' => 'Contact updated and saved.',
		'return' => '/address-book'
	]);
}

$current_user = wp_get_current_user(); // get the current User Object
$context['current_user'] = $current_user->display_name;

$context['send_media_form'] = gravity_form( 1, false, false, false, '', false, 4, false );

the_post();

// attempt to render a corresponding view (twig) file that matches any of the below patterns
// if no matches are found, load the default post view (single.twig)
Timber::render([
	'single-' . $post->ID . '.twig',
	'single-' . $post->post_type . '.twig',
	'single.twig'
], $context);