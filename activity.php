<?php
// Template Name: All Claims Template

$context = Timber::get_context();
$post = Timber::get_post();
$context['post'] = $post;

$current_user = wp_get_current_user();

// get the current users posts
$context['claims'] = Timber::get_posts([
	'post_type' => 'claim',
	'posts_per_page' => -1,
	'post_status' => 'publish',
	'author' => $current_user->ID, // explicitly set the author
	'facetwp' => true
]);

$templates = ['all-claims.twig'];

Timber::render( $templates, $context );