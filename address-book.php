<?php
// Template Name: Address Book Template

$context = Timber::get_context();
$post = Timber::get_post();
$context['post'] = $post;

// get the current User object
$current_user = wp_get_current_user();

// get the current users posts
$context['address_book'] = Timber::get_posts([
	'post_type' => 'addressbook',
	'posts_per_page' => -1,
	'post_status' => 'publish',
	'order' => 'ASC',
	'orderby' => 'title',
	'author' => $current_user->ID
]);

$templates = ['address-book.twig'];

Timber::render( $templates, $context );