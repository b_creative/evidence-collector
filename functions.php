<?php

// change 'views' directory to 'templates'
Timber::$locations = __DIR__ . '/templates';

class MODSite extends TimberSite {

	/**
	 * To add items to the site class, first add an action hook to the constructor, plus your custom function name
	 * then create your function outside of the constructor

	 * To add global actions or filters, put both the hook and function outside of this site class
	 * https://timber.github.io/docs/reference/timber-post/#__construct
	*/

	function __construct() {
		// Action Hooks //
		add_action( 'after_setup_theme', [ $this, 'after_setup_theme' ] );
		add_action( 'wp_enqueue_scripts', [ $this, 'enqueue_scripts' ] );
		add_action( 'admin_head', [ $this, 'admin_head_css' ] );
		add_action( 'admin_menu', [ $this, 'admin_required' ] );
		add_action( 'admin_init', [ $this, 'admin_required_init' ] );
		add_action( 'init', [ $this, 'register_post_types' ] );
		add_action( 'login_enqueue_scripts', [ $this, 'style_login' ] );
		add_action( 'check_admin_referer', [ $this, 'logout_without_confirm' ], 10, 2);

		// Filter Hooks //
		add_filter( 'timber_context', [ $this, 'add_to_context' ] );
		add_filter( 'manage_pages_columns', [ $this, 'remove_pages_count_columns' ] );
		add_filter( 'show_admin_bar', [ $this, '__return_false' ] );
		add_filter( 'image_strip_meta', [ $this, false ] );

		parent::__construct();
	}

	// hide nags and unused items
	function admin_head_css() {
		?>
		<style type="text/css">
			.update-nag,
			#wp-admin-bar-comments,
			#wp-admin-bar-new-content,
			#wp-admin-bar-videowhisper,
			.fs-modal { display: none !important; }
		</style>
		<?php
	}

	// WP admin login styles
	function style_login() {
		?>
		<style type="text/css">
			#login h1, .login h1 {
				background-color: #EC6212;
				padding: 1.5rem 0.5rem;
				border-radius: 3px;
			}

			#login h1 a, .login h1 a {
				background-image: url('<?= get_stylesheet_directory_uri() . '/assets/media/ec-logo.png' ?>') !important;
				background-position: center;
				width: 14rem;
				background-size: contain;
				margin: 0 auto;
			}
		</style>
		<?php
	}

	// enqueue styles & scripts
	function enqueue_scripts() {
		$version = filemtime( get_stylesheet_directory() . '/style.css' );
		wp_enqueue_style( 'core-css', get_stylesheet_directory_uri() . '/style.css', [], $version );
		wp_enqueue_script( 'geo-js', get_template_directory_uri() . '/assets/js/dist/geo-js-dist.js', [], $version );
		// the above will always load & run

		// selective load from here down so we don't run unecessary scripts
		if( is_singular('claim') ) {
			wp_enqueue_script( 'mfp-js', get_template_directory_uri() . '/assets/js/dist/mfp.js', [], $version );
		}

		if( is_page('group-claims') ) {
			wp_enqueue_script( 'datatables', get_template_directory_uri() . '/assets/js/dist/datatables.min.js', [], $version );
		}

		wp_enqueue_script( 'mod-js', get_template_directory_uri() . '/assets/js/site-dist.js', ['jquery', 'jquery-ui-tabs'], $version );

		// pass PHP data to JS
		wp_localize_script( 'mod-js', 'users', [ 'current_user' => wp_get_current_user() ] );
	}

	// Custom context helper functions (callable)
	function add_to_context( $context ) {
		$context['site']           = $this; // the current site class
		$context['date']           = date('F j, Y');
		$context['date_year']      = date('Y');
		$context['options']        = get_fields('option');
		$context['home_url']       = home_url('/');
		$context['is_front_page']  = is_front_page();
		$context['get_url']        = $_SERVER['REQUEST_URI'];

		return $context;
	}

	// Menus / Theme Support / ACF Options Page
	function after_setup_theme() {
		register_nav_menu( 'primary', 'Primary Navigation' );
		register_nav_menu( 'primary-mobile', 'Primary Navigation - Mobile' );
		register_nav_menu( 'secondary', 'Secondary Navigation' );
		add_theme_support( 'menus' );
		add_theme_support( 'align-wide' );
		add_theme_support( 'editor-styles' );
		add_theme_support( 'disable-custom-colors' ); // disable color picker wheel

		// create option pages for things like footer data and company info/logos
		if( function_exists( 'acf_add_options_page' ) ) {
			$parent = acf_add_options_page([
				'page_title'      => 'Main Options',
				'menu_title'      => 'Theme Options',
				'capability'      => 'edit_posts',
				'redirect'        => false,
				'icon_url'		  => 'dashicons-database-view',
				'position'		  => 2,
				'updated_message' => 'Core options updated.',
			]);

			// Company Settings
			$child = acf_add_options_sub_page([
				'page_title'  => __( 'Company Settings' ),
				'menu_title'  => __( 'Companies' ),
				'parent_slug' => $parent['menu_slug'],
				'updated_message' => 'Company selection list updated.',
			]);

			// Contact Settings
			$child = acf_add_options_sub_page([
				'page_title'  => __( 'Contact Settings' ),
				'menu_title'  => __( 'Contact' ),
				'parent_slug' => $parent['menu_slug'],
				'updated_message' => 'Contact settings updated.',
			]);
		}
	}

	// include post types
	function register_post_types() {
		include_once('custom-post-types/post-type-claims.php');
		include_once('custom-post-types/post-type-addressbook.php');
	}

	// removed comment column from posts pages
	function remove_pages_count_columns( $defaults ) {
		unset($defaults['comments']);
		return $defaults;
	}

	// To see these menu items, you need Admin or a specified name to access
	function admin_required() {
		if( is_admin() ) {
			if( !current_user_can('administrator') ) {
				remove_menu_page('tools.php'); // Tools
				remove_menu_page('edit.php?post_type=page'); // Pages
				remove_menu_page('edit.php?post_type=addressbook'); // Address Book
			}

			// remove for all users (disable)
			remove_menu_page( 'edit.php' ); // Posts
			remove_menu_page( 'edit-comments.php' ); // Comments
			remove_menu_page( 'edit.php?post_type=pg_groupwalls' ); // ProfileGrid Group Walls
			remove_menu_page( 'edit.php?post_type=profilegrid_blogs' ); // ProfileGrid Blog
		}
	}

	// Extends the function above to run on the admin_init hook
	function admin_required_init() {
		if( is_admin() && !current_user_can('administrator') ) {
			remove_menu_page( 'acf-options-theme-options' ); // ACF Options
		}
	}

	// logout without confirmation
	function logout_without_confirm( $action, $result ) {
		if( $action == "log-out" && !isset( $_GET['_wpnonce'] ) ) {
			$redirect_to = isset( $_REQUEST['redirect_to'] ) ? $_REQUEST['redirect_to'] : 'log-out';
			$location = str_replace( '&amp;', '&', wp_logout_url( $redirect_to ) );
			header( "Location: $location" );
			die;
		}
	}
} // End of MODSite class

new MODSite();

// main site nav - used to render the top left and bottom main menus
function mod_render_primary_menu() {
	wp_nav_menu([
		'theme_location' => 'primary',
		'container'      => false,
		'menu_id'        => 'primary-menu',
	]);
}

function mod_render_primary_mobile_menu() {
	wp_nav_menu([
		'theme_location' => 'primary-mobile',
		'container'      => false,
		'menu_id'        => 'primary-menu-mobile',
	]);
}

// used to render the Privacy, Legal, etc in the footer
function mod_render_secondary_menu() {
	wp_nav_menu([
		'theme_location' => 'secondary',
		'container'      => false,
		'menu_id'        => 'secondary-menu',
	]);
}

// disable the Gutenberg on certain templates
function mod_disable_editor( $id = false ) {
	$excluded_templates = [
		'front-page.php',
		'my-claims.php',
		'all-claims.php',
		'group-claims.php'
	];

	// page must have an ID
	if( empty( $id ) )
		return false;

	// do not allow ID to be ambiguous data type
	$id = intval( $id );
	$template = get_page_template_slug( $id );

	// return the ID's associated with the templates we want to exclude from Gutenberg
	return in_array( $template, $excluded_templates );
}

function mod_disable_gutenberg( $can_edit, $post_type ) {
	// make sure we're in the right admin section
	if( ! ( is_admin() && !empty( $_GET['post'] ) ) )
		return $can_edit;

	// get our excluded template to edit without Gutenberg
	if( mod_disable_editor( $_GET['post'] ) )
		$can_edit = false;

	return $can_edit;
}
add_filter( 'gutenberg_can_edit_post_type', 'mod_disable_gutenberg', 10, 2 );
add_filter( 'use_block_editor_for_post_type', 'mod_disable_gutenberg', 10, 2 );

// adds an edit action to the members section in ProfileGrid
do_action('profilegrid_dashboard_user_manager_action_area');

// set the post author to the rep chosen rather than the current user (runs after the claim is created)
add_action('acf/save_post', 'acf_claim_builder_save');
function acf_claim_builder_save( $post_id ) {
	$post_type = get_post_type($post_id);

	if( $post_type == 'claim' ) {
		$rep_id = get_field('claim_rep_select', $post_id);
		$claim_number = get_field('claim_number', $post_id);
		$claim_title = get_field('claim_title', $post_id);

		if( empty( $claim_title ) || $claim_title == '' ) {
			$claim_title = 'Claim ' . $claim_number;
		}

		// add/update the post id, post title, and rep ID
		$args = [
			'ID' => $post_id,
			'post_type' => 'claim',
			'post_title' => $claim_title,
			'post_author' => $rep_id['ID']
		];

		// wp_update_post( [ $args, $attachments ] );
		wp_update_post( $args );
	}
}

// Save addressbook to the creating user (explicitly)
add_action( 'acf/save_post', 'acf_addressbook_modifier' );
function acf_addressbook_modifier( $post_id ) {
	$post_type = get_post_type($post_id);
	$current_user = wp_get_current_user();
	$email = get_field( 'email', $post_id );

	if( $post_type == 'addressbook' ) {
		$args = [
			'ID' => $post_id,
			'post_type' => 'addressbook',
			'post_title' => $email, // ensures post title is unique
		];
		wp_update_post( $args );
	}
}

function edit_address_book() {
	$current_user = wp_get_current_user();

	acf_form([
		'id' => 'create-contact-form',
		'post_id' => 'new_post',
		'new_post' => [
			'post_type' => 'addressbook',
			'post_status' => 'publish',
			'author' => $current_user->ID,
		],
		'submit_value' => __('Add Contact', 'acf'),
		'updated_message' => 'Contact added.',
	]);
}

// populate the frontend claim form with companies in the dropdown
function ec_populate_companies( $field ) {
	$field['choices'] = []; // the key for the ACF Select field will always be 'choices'

	// get the sub-field value from our repeater object
	if( have_rows('ec_company_names', 'option') ) {
		while( have_rows('ec_company_names', 'option') ) {
			the_row();

			$select_val = get_sub_field('ec_company', 'option', false);
			$label = $select_val; // set label to the value

			strtolower($select_val); // lowercase the return value for formatting

			$field['choices'][$select_val] = ucfirst($label);
		}
	}

	return $field;
}
add_filter( 'acf/load_field/name=company_name', 'ec_populate_companies' );

// populate gform checkbox fields (1 - form id)
add_filter( 'gform_pre_render_1', 'render_image_ops' );
add_filter( 'gform_pre_validation_1', 'render_image_ops' );
add_filter( 'gform_pre_submission_filter_1', 'render_image_ops' );
add_filter( 'gform_admin_pre_render_1', 'render_image_ops' );
function render_image_ops( $form ) {

	// pass our field by reference so we can modify the returned values
	foreach( $form['fields'] as &$field ) {
		$field_id = 18; // ID of the checkbox field

		if( $field->id != $field_id ) {
			continue; // quit execution and go to the outer loop to get the next field ID
		}

		// get all of our claims
		$posts = get_posts([
			'post_type' => 'claim',
			'posts_per_page' => -1,
			'post_status' => 'publish'
		]);

		$input_id = 1;
		foreach( $posts as $post ) {

			// skip multiples of 10 for input ids
			if( $input_id % 10 == 0 ) {
				$input_id++;
			}

			if( $post->ID == get_the_ID() ) { // check if the ID matches the current post ID in the loop
				include('media-loop.php');
			}
		}

		$field->choices = $choices;
		$field->inputs = $inputs;
	}

	return $form;
}