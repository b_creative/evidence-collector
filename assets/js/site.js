// Author: Shane Schroll | Baseline Creative
// ESLint is active and will enforce JSX code standards in this file
(function($) {
    $(document).ready(function() {

		// globals
		let $document = $(document);
		let $window = window;
		let $body = $('body');
		acf.unload.active = false;

		var get_profile_url = $window.location.href.indexOf('profile');

		if( get_profile_url > -1 ) {
			// prepend address book link
			$('#pg-profile-tabs #pg-groups').
			prepend('<a class="button--redorange btn-adjust" href="/address-book">Address Book</a>');

			// prepend users claims link
			$('#pg-profile-tabs #pg-groups').
			prepend('<a class="button--redorange btn-adjust" href="/my-claims">My Claims</a>');

			// prepend admin dashboard link for specified users (uses wp_localize_script)
			var userID = users.current_user.ID;
			if( userID === 14 || userID === 4 ) {
				$('#pg-profile-tabs #pg-groups').
				prepend('<a class="button--redorange btn-adjust" href="/wp-admin">Admin Dashboard</a>');
			}
		}

		// text for when a user tries to login without being approved
		$('.pm-login-box-error span').text('Your account must be approved before you can login.');

		// #menu-item-204 is the logout <li>
		$(function showLogout() {
			$('#menu-item-204').hide();

			if( $body.hasClass('logged-in') ) {
				$('#menu-item-204').show();
			}
		});

		// for the New Claim / Active Claim / Completed Claim Tabs
		$('#tabs').tabs();

		// New Claim Tab
		$('#tabs .tab1').click(function() {
			$(this).addClass('active');
			$('#tabs .tab2').removeClass('active');
			$('#tabs .tab3').removeClass('active');
		});

		// Active Claim Tab
		$('#tabs .tab2').click(function() {
			$(this).addClass('active');
			$('#tabs .tab1').removeClass('active');
			$('#tabs .tab3').removeClass('active');
		});

		// Completed Claim Tab
		$('#tabs .tab3').click(function() {
			$(this).addClass('active');
			$('#tabs .tab1').removeClass('active');
			$('#tabs .tab2').removeClass('active');
		});

		// FacetWP live refresh with spinner
		$(function customFacetSettings() {
			$('#reset-tabs').hide();
			$('.search-results').hide();

			$document.on('facetwp-refresh', function() {
				$('.facetwp-template').prepend('<div class="is-loading"><span class="is-loading__icon"></span></div>');
				$('#tabs').fadeOut(350);
				$('.claim-group-wrapper').fadeOut(350);
				$('.search-results').fadeIn(450);
			});

			// once a search is made and results are loaded, remove the loading spinner
			// this function runs when the page is initially loaded and each time new results are loaded
			$document.on('facetwp-loaded', function() {
				$('.facetwp-template .is-loading').remove();
				$('#reset-tabs').show();
			});

			$('#reset-tabs').click(function() {
				FWP.reset();
				$('.search-results').fadeOut(350);
				$('#tabs').fadeIn(450);
				$('.claim-group-wrapper').fadeIn(450);
			});
		});

		// close notification on "My Claims" page
		$('.assignment-info__close').click(function() {
			$(this).parent().fadeOut(350);
		});

		// preserve $(window) instead of using $window, otherwise a Deferred Exception will be thrown
		if( $(window).width() < 810 ) {
			$('#user_login').focus(function() {
				$('.nav-wrapper').hide();
			});

			$('#user_login').focusout(function() {
				$('.nav-wrapper').show();
			});
		}

		$('#user_pass').after('<span id="view-pass" class="fa-regular fa-eye" title="Show Password"></span>');

		// show/hide password (password data is still hashed)
		$('#view-pass').click(function() {
			$(this).toggleClass('fa-eye');
			var val = $('#user_pass') ? 'text' : 'password';
            $('#user_pass').attr('type', val);
		});

		// claim address book buttons and events
		$(function claimAddressBook() {
			$('#field_1_17').append('<a id="add-contact" class="button--redorange"><span class="fa-solid fa-plus"></span> Add Contact</a>');

			$('#add-contact').click(function() {
				$('.address-book-modal').fadeIn(350);
				$('.address-book-modal #message').hide();
			});

			$('.close-modal').click(function() {
				$('.address-book-modal').fadeOut(350);
			});
		});

		// accessible accordion block - controls and aria events for screenreaders
		$(function accordionBlock() {
			var uploaded_items = $('.expander');
			var label = $('.icon-header');

			$('.expander').each(function() {
				$(this).hide();
			});

			label.click(function() {
				var $this = $(this); // shortener

				if( $this.hasClass('target') ) {
					$this.removeClass('target');
					$this.attr('aria-pressed', 'true');
					$this.next(uploaded_items).slideToggle(350);
					$this.next(uploaded_items).attr('aria-expanded', 'true');
				} else {
					$this.next('.expander:first').slideToggle(350, () => {
						$this.prev(label).addClass('target');
						$this.prev(label).attr('aria-pressed', 'false');
						$this.attr('aria-expanded', 'false');
					});
				}
			});
		});

		// // if we are making a claim, modify the input fields
		if( $body.hasClass('page-template-make-a-claim') ) {
			var photo = $('#photo_upload_link input[type="file"]');
			var video = $('#video_upload_link input[type="file"]');
			var audio = $('#audio_upload_link input[type="file"]');

			photo.attr({
				'value': 'Take Photo',
				'accept': 'image/*',
				'capture': 'environment'
			});

			video.attr({
				'value': 'Take Video',
				'accept': 'video/*',
				'capture': 'environment'
			});

			audio.attr({
				'value': 'Capture Recording',
				'accept': 'audio/*',
				'capture': 'user'
			});
		}
	}); // end Document.Ready
})(jQuery);