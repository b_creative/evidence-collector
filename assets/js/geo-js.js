// Author: Shane Schroll | Baseline Creative
(function($) {
    $(document).ready(function() {

		var api_key = '8223bf7ded664aaba25eb524c02cebb2';

		// prompt user to accept Location Access on their device
		if( navigator.geolocation ) {

			// user accepted location access
			function success(position) {
				const latitude = position.coords.latitude;
				const longitude = position.coords.longitude;

				// URL for geolocation API
				var api_url = 'https://api.opencagedata.com/geocode/v1/json';

				// setup the API call parameters
				var request_url = api_url
				+ '?'
				+ 'key=' + api_key
				+ '&q=' + encodeURIComponent(latitude + ',' + longitude)
				+ '&pretty=1'
				+ '&no_annotations=1';

				// request the API and save the response
				var request = new XMLHttpRequest();
				request.open('GET', request_url, true);

				// handle the response and check the status (200 success / 500 error)
				request.onload = function() {
					if(request.status === 200) {
						var data = JSON.parse(request.responseText);
						var location = data.results[0].formatted;

						// output the location next to the media
						$('.gps-data').text(location);

						// save the location data for the claim to the database
						var field = acf.getField('field_62563f9887aef');
						if( ! field.val() ) {
							field.val(location);
						} else {
							field.disable(); // prevent value from being overwritten once set
						}

					} else if(request.status <= 500) {
						console.error('Unable to geocode. Response code: ' + request.status);
						var data = JSON.parse(request.responseText);
						console.error('Parse error: ' + data.status.message);
					} else {
						console.error('Server timeout.');
					}
				};

				// Connection error
				request.onerror = function() {
					console.log("Unable to connect to server.");
				};

				// make the request
				request.send();
			}

			// user denied location access OR location access is not supported
			function error() {
				console.log('Location access denied.');
			}

			// Request Options
			const options = {
				enableHighAccuracy: true,
				maximumAge: 30000,
				timeout: 27000
			};

			// Request Location
			navigator.geolocation.getCurrentPosition(success, error, options);
		}
	}); // end Document.Ready
})(jQuery);